package cz.cejka.tracker;

import cz.cejka.tracker.model.PaymentRecords;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Payment tracker for reading payments from file and console input
 * Created by josefcejka.
 */
public class PaymentTracker {

    /**
     * Input pattern
     */
    private final Pattern pattern = Pattern.compile("([A-Z]{3}) ((\\-)?(0|([1-9][0-9]*)))");

    /**
     * Collected payment records
     */
    private PaymentRecords paymentRecords;

    /**
     * Thread to print payment records
     */
    private Thread recordsPrint;

    /**
     * Main method to start application
     *
     * @param args
     */
    public static void main(String[] args) {
        new PaymentTracker();

    }

    /**
     * Constructor initialize payment record class and start to print records to console
     * also input reader is started
     */
    public PaymentTracker() {
        paymentRecords = new PaymentRecords(new HashMap<>());
        startRecordsPrint();
        readInput();
    }

    /**
     * Read input from console, first line is optional filename,
     * if filename is blank on doesnt exist next imput is parsed and if is valid then payment is added
     */
    private void readInput() {
        System.out.println("If you would like to read input from file please enter filename:");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        if (!"".equals(input)) {
            getFile(input);

        }
        System.out.println("Enter your payments:");
        do {
            input = sc.nextLine();
            parseInput(input);
        } while (!input.equals("quit"));
        recordsPrint.interrupt();
        sc.close();
    }

    /**
     * Input is parsed by pattern is added to payments or mark as invalid,
     * if input contains "quit" program is terminated
     *
     * @param input
     */
    private void parseInput(String input) {
        String currency = "";
        try {
            if ((pattern.matcher(input).matches())) {
                Matcher matcher = pattern.matcher(input);
                if (matcher.find()) {
                    currency = matcher.group(1);
                    int amount = Integer.parseInt(matcher.group(2));
                    paymentRecords.addPayment(currency, amount);
                    System.out.println("Payment has been added.");
                }
            } else if (!input.equals("quit")) {
                System.out.println("Invalid input format.");
            }
        } catch (IllegalArgumentException e) {
            System.err.format("%s: currency does not exist%n", currency);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Loading file by filename from resouces folder and read lines are send to parsing
     *
     * @param fileName
     */
    private void getFile(String fileName) {
        try {
            Path path = Paths.get(fileName);
            List<String> lines = Files.readAllLines(path);
            for (String line : lines) {
                parseInput(line);
            }
        } catch (NoSuchFileException e) {
            System.err.format("%s: no such" + " file or directory%n", fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initialization of tread for console print
     */
    private void startRecordsPrint() {
        recordsPrint = new Thread(paymentRecords);
        recordsPrint.start();
    }

}
