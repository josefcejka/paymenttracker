package cz.cejka.tracker.model;

import cz.cejka.tracker.utils.CurrencyConverter;

import java.io.IOException;

/**
 * Class storage payment record - info about currency, amount and default rate
 * Created by josefcejka.
 */

public class Record {

    private final String DEFAULT_CURRENCY = "USD";

    private float rate;

    private String currency;

    private int amount;

    /**
     *
     * @param currency
     * @param amount
     * @throws IOException
     */
    public Record(String currency, int amount) throws IOException {
        this.currency = currency;
        this.amount = amount;
        this.rate = CurrencyConverter.getConversionRate("USD", currency);
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void addAmount(int payment){
        this.amount += payment;
    }


    public void print() {
        System.out.printf("%s %d (%s %.2f)\n", currency, amount, DEFAULT_CURRENCY, amount/rate);
    }

    @Override
    public String toString() {
        return "Record{" +
                "DEFAULT_CURRENCY='" + DEFAULT_CURRENCY + '\'' +
                ", rate=" + rate +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                '}';
    }
}
