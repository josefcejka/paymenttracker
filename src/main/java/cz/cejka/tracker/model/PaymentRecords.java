package cz.cejka.tracker.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Class collect payments - currency and amount
 * Created by josefcejka.
 */
public class PaymentRecords implements Runnable {

    private static long INTERVAL = 60000;

    private HashMap<String, Record> payments;

    /**
     * Constructor
     * @param payments
     */
    public PaymentRecords(HashMap<String, Record> payments) {
        this.payments = payments;
    }

    /**
     * Adding amount and currency
     * @param currency
     * @param amount
     */
    public void addPayment(String currency, int amount) throws IOException {
        if (payments.containsKey(currency)) {
            Record record = payments.get(currency);
            record.addAmount(amount);
            payments.put(currency, record);
        } else {
            payments.put(currency, new Record(currency,amount));
        }
    }

    /**
     * Print payments to console
     */
    public void printPayments() {
        for (Map.Entry<String, Record> e : payments.entrySet()) {
            if(e.getValue().getAmount() != 0)
                e.getValue().print();
        }
    }

    /**
     * Running printing thread to console
     */
    public void run() {
        while (true) {
            this.printPayments();
            try {
                Thread.sleep(INTERVAL);
            } catch (InterruptedException e) {
            }
        }
    }
}
