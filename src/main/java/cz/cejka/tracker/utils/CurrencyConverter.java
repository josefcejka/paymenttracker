package cz.cejka.tracker.utils;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

/**
 * Class for work with currency conversion
 * Created by josefcejka.
 */
public class CurrencyConverter {

    /**
     * Downloading current rate
     * @param from
     * @param to
     * @return
     * @throws IOException
     */
    public static float getConversionRate(String from, String to) throws IOException
    {
        HttpClientBuilder builder = HttpClientBuilder.create();
        try (CloseableHttpClient httpclient = builder.build())
        {
            HttpGet httpGet = new HttpGet("http://quote.yahoo.com/d/quotes.csv?s=" + from + to + "=X&f=l1&e=.csv");
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            String responseBody = httpclient.execute(httpGet, responseHandler);
            return Float.parseFloat(responseBody);
        }
    }

}

