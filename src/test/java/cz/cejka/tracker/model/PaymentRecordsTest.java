package cz.cejka.tracker.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by cejka.
 */
public class PaymentRecordsTest {

    HashMap<String, Record> payments;
    PaymentRecords paymentRecords;

    @Before
    public void init() {
        this.payments = new HashMap<>();
        this.paymentRecords = new PaymentRecords(payments);
        initRecords();
    }

    private void initRecords() {
        try {
            paymentRecords.addPayment("USD", 100);
            paymentRecords.addPayment("USD", -100);
            paymentRecords.addPayment("CZK", 100);
            paymentRecords.addPayment("CZK", 1000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addPayment() throws IOException {
        assertEquals(payments.get("USD").getAmount(), 0);
        assertEquals(payments.get("CZK").getAmount(), 1100);
        assertEquals(payments.size(), 2);
        paymentRecords.addPayment("EUR", 50);
        paymentRecords.addPayment("EUR", 50);
        assertEquals(payments.size(), 3);
    }


    @Test(expected = IllegalArgumentException.class)
    public void addPaymentThrowsException() throws IOException {
        paymentRecords.addPayment("ABC", 21);
    }

}